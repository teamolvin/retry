const { expect } = require('chai');

const retry = require('../index');

describe('retry', () => {
  it('retries', async () => {
    let callCount = 0;
    const testLimit = 2;
    const cb = () => {
      callCount += 1;
      if (callCount < testLimit) {
        throw new Error();
      }
      return callCount * 2;
    };

    const retryPromise = retry(cb, testLimit + 1, 1);
    const results = await retryPromise;
    expect(results).to.equal(testLimit * 2);
  });

  it('allows for external exception logging', async () => {
    let callCount = 0;
    const testLimit = 2;
    const err = new Error();
    const cb = () => {
      callCount += 1;
      if (callCount < testLimit) {
        throw err;
      }
      return callCount * 2;
    };
    let loggedErr;

    const retryPromise = retry(cb, testLimit + 1, 1, logErr => {
      loggedErr = logErr;
    });
    const results = await retryPromise;
    expect(loggedErr).to.equal(err);
    expect(results).to.equal(testLimit * 2);
  });

  it('retries until limit', async () => {
    const testLimit = 2;
    const err = new Error();
    let thrown;
    const cb = () => {
      throw err;
    };

    let results;
    try {
      const retryPromise = retry(cb, testLimit + 1, 1);
      results = await retryPromise;
    } catch (e) {
      thrown = e;
    }
    expect(results).to.be.undefined; // eslint-disable-line no-unused-expressions
    expect(thrown).to.equal(err);
  });

  it('retries until limit', async () => {
    const testLimit = 2;
    const err = null;
    let thrown;
    const cb = () => {
      throw err;
    };

    let results;
    try {
      const retryPromise = retry(cb, testLimit + 1, 1);
      results = await retryPromise;
    } catch (e) {
      thrown = e;
    }
    expect(results).to.be.undefined; // eslint-disable-line no-unused-expressions
    expect(thrown).to.be.an.instanceof(Error);
  });

  it('retries until default limit', async () => {
    const err = new Error();
    let callCount = 0;
    let thrown;
    const cb = () => {
      callCount += 1;
      throw err;
    };

    let results;
    try {
      results = await retry(cb, undefined, 1);
    } catch (e) {
      thrown = e;
    }
    expect(results).to.be.undefined; // eslint-disable-line no-unused-expressions
    expect(thrown).to.equal(err);
    expect(callCount).to.equal(retry.DEFAULT_RETRY_COUNT);
  });

  it('Logs retries, if requested', async () => {
    let callCount = 0;
    const testLimit = 2;
    const cb = () => {
      callCount += 1;
      if (callCount < testLimit) {
        throw new Error();
      }
      return callCount * 2;
    };
    let logCount = 0;

    retry.setLog(() => {
      logCount += 1;
    });

    const retryPromise = retry(cb, testLimit + 1, 1);
    const results = await retryPromise;
    expect(results).to.equal(testLimit * 2);
    expect(logCount).to.equal(testLimit - 1);
  });
});
