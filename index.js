/* eslint-disable no-await-in-loop */
const DEFAULT_RETRY_COUNT = 5;
const DEFAULT_RETRY_WAIT_MS = 2000;
let log = null;
const sleep = async ms => new Promise(resolve => setTimeout(resolve, ms));

const retry = async (
  callback,
  maxAttempts = DEFAULT_RETRY_COUNT,
  // istanbul ignore next
  retryWaitMs = DEFAULT_RETRY_WAIT_MS,
  exceptionLogger = null
) => {
  let lastExceptionCaught;
  for (let attemptNo = 0; attemptNo < maxAttempts; attemptNo += 1) {
    try {
      // note: the `await` is NOT superfluous;
      // without it the exceptions would not be caught.
      return await callback();
    } catch (currentException) {
      lastExceptionCaught = currentException;
    }
    if (exceptionLogger) {
      exceptionLogger(lastExceptionCaught, attemptNo);
    }
    if (log) {
      log('Retrying', {
        attemptNo,
        maxAttempts,
        message: lastExceptionCaught.message,
        code: lastExceptionCaught.code,
        stack: lastExceptionCaught.stack
      });
    }
    await sleep(retryWaitMs * 2 ** attemptNo);
  }
  throw lastExceptionCaught || new Error('Retry failed without clear error');
};

retry.setLog = inLog => {
  log = inLog;
};

retry.DEFAULT_RETRY_COUNT = DEFAULT_RETRY_COUNT;

module.exports = retry;
